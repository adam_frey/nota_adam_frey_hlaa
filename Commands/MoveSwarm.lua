function getInfo()
	return {
		tooltip = "Move units to given position",
		parameterDefs = {
			{ 
				name = "units",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "pos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	local r = 20
	-- get position of unit
	local units = parameter.units -- array of unit ids
	local pos = parameter.pos -- Vec3
	-- compute distance
	delta = 0
	for i=1, #units do
		local x, y, z = Spring.GetUnitPosition(units[i])
		if x ~= nil then
			local d = math.sqrt((pos.x - x) * (pos.x - x) + (pos.z - z) * (pos.z - z))	
			delta = delta + d
		end
	end
	local delta = delta/#units
	-- check success or act
	local max_delta = 300
	if (delta < max_delta) then
		return SUCCESS
	else
		for i=1, #units do
			x = pos.x + i * r - r * (#units/2)
			z = pos.z - i * r + r * (#units/2)
			if x ~= nil then
				--SpringGiveOrderToUnit(units[i],CMD.FIRE_STATE,{0},{})
				SpringGiveOrderToUnit(units[i], CMD.MOVE, {x,pos.y,z}, {})
			end
		end
	end
	return RUNNING
end
