function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Form line from units between two points",
		parameterDefs = {
			{ 
				name = "point_a",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "point_b",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	local a = parameter.point_a -- Vec3
	local b = parameter.point_b -- Vec3
	-- get target locations and issue commands 
	if self.dest == nil then
		self.units = units
		self.dest = {}
		local d = (b-a) / (#self.units-1)
		for i=1, #self.units do
			local v = a + d * (i-1)
			v = v:AsSpringVector()
			v[2] = Spring.GetGroundHeight(v[1],v[3])
			 self.dest[i] = Vec3(v[1],v[2],v[3])
			SpringGiveOrderToUnit(self.units[i], CMD.MOVE, self.dest[i]:AsSpringVector(), {})
		end
	end
	if self.finished == nil then
		self.finished = {}
	end
	-- are we there yet?
	local done = true
	for i=1, #self.units do
		local x,y,z = SpringGetUnitPosition(self.units[i])
		local pos = Vec3(x,y,z)
		local dest = self.dest[i]
		if (Spring.GetUnitIsDead(self.units[i]) or pos:Distance(dest) <= 500) then
			self.finished[i] = true
		end
		if (self.finished[i] ~= true) then
			done = false
		end
	end
	-- return state
	if done then
		return SUCCESS
	else
		return RUNNING
	end
end
