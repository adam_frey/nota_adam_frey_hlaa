function getInfo()
	return {
		tooltip = "Move units to given position",
		parameterDefs = {
			{ 
				name = "units",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	local function lane2arr(lane)
		local arr = {}
		for i=1,#lane do
			arr[#arr+1] = lane[i]["position"]
		end
		return arr
	end
	-- get path
	local points = lane2arr(parameter.path)
	local p = {}
	for i=1,#points-1 do
		local p1 = points[i]
		local p2 = points[i+1]
		p[#p+1] = p1
		for j=1,2 do -- add 2 points
			p[#p+1] = p1 + (p2-p1)*(j/2)
		end
	end
	p[#p+1] = points[#points]
	local path = p
	-- get position of unit
	local units = parameter.units -- array of unit ids
	-- compute distance
	local delta = 0
	local destination = path[#path]
	for i=1, #units do
		local x, y, z = Spring.GetUnitPosition(units[i])
		local d = destination:Distance(Vec3(x,y,z))
		delta = delta + d
	end
	local delta = delta/#units
	-- check success or act
	local max_delta = 125
	if (delta < max_delta) then
		return SUCCESS
	else
		for i=1, #units do
			-- (try to) reclaim everything on the path
			-- SpringGiveOrderToUnit(units[i], CMD.MOVE, path[1]:AsSpringVector(), {})
			SpringGiveOrderToUnit(units[i], CMD.RECLAIM, {path[1].x, path[1].y, path[1].z, 900}, {})
			for j=2, #path do
				SpringGiveOrderToUnit(units[i], CMD.RECLAIM, {path[j].x, path[j].y, path[j].z, 900}, {"shift"})
			end
		end
	end
	return RUNNING
end
