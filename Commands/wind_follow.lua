function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Leader follows the wind, everyone else follows the leader.",
		parameterDefs = {
			{ 
				name = "windx",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "windz",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	-- get params
	local wind_x = parameter.windx
	local wind_z = parameter.windz
	
	for i=1, #units do
		if (Spring.GetUnitDefID(units[i]) == 31) then
			leader = units[i]
		end
	end
	
	local pointX, pointY, pointZ = SpringGetUnitPosition(leader)
	local leaderPosition = Vec3(pointX, pointY, pointZ)

	-- check leader success
	if (self.position ~= nil and leaderPosition:Distance(self.position) < 100) then
		self.position = nil
		return SUCCESS
	else
		-- assign move command to leader
		if (self.position == nil) then
			self.position = Vec3(pointX + 150 * wind_x, pointY, pointZ + 150 * wind_z)
			SpringGiveOrderToUnit(leader, CMD.MOVE, self.position:AsSpringVector(), {})
		end
		
		local follower_count = #units - 1
		for i=1, #units do
			-- assign move command to a follower
			if (units[i] ~= leader) then 
				local offset = Vec3(100 * math.sin(i * 2 * math.pi / follower_count), 
									0, 
									100 * math.cos(i * 2 * math.pi / follower_count))
				local follower_position = leaderPosition + offset
				SpringGiveOrderToUnit(units[i], CMD.MOVE, follower_position:AsSpringVector(), {})
			end
		end
	end
	
	return RUNNING
end

