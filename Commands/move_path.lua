function getInfo()
	return {
		tooltip = "Move unit to given position",
		parameterDefs = {
			{ 
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	-- get position of unit
	local unit = parameter.unit
	local uX, uY, uZ = SpringGetUnitPosition(unit)
	local start = Vec3(uX, uY, uZ)
	local path = parameter.path
	-- run commands
	if self.done == nil then
		self.done = true
		-- compute path
		-- follow path
		SpringGiveOrderToUnit(unit, CMD.MOVE, path[1]:AsSpringVector(), {})	
		for i=2, #path do
			SpringGiveOrderToUnit(unit, CMD.MOVE, path[i]:AsSpringVector(), {"shift"})	
		end
	end
	-- return result
	local t = path[#path]
	local d = math.sqrt((uX - t.x) * (uX - t.x) + (uZ - t.z) * (uZ- t.z))
	if (d < 150) then
		return SUCCESS
	else
		return RUNNING
	end
end

function Reset(self)
	self.done = nil
end