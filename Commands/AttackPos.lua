function getInfo()
	return {
		tooltip = "kill",
		parameterDefs = {
			{ 
				name = "units",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "pos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	local r = 5
	-- get position of unit
	local u = parameter.units -- array of unit ids
	local pos = parameter.pos -- Vec3
	-- end?
	local c = 5
	local r = Spring.GetUnitsInBox(pos.x-c,pos.y-100,pos.z-c,pos.x+c,pos.y+100,pos.z+c)
	if #r == 0 then
		for i=1,#u do
			--SpringGiveOrderToUnit(u[i],CMD.FIRE_STATE,{2},{})	
		end
		return SUCCESS
	end
	-- attack
	for i=1,#u do
		local x,y,z = SpringGetUnitPosition(u[i])
		if Vec3(x,y,z):Distance(pos) <= 960 then
			SpringGiveOrderToUnit(u[i],CMD.FIRE_STATE,{1},{})
			SpringGiveOrderToUnit(u[i],CMD.ATTACK,pos:AsSpringVector(),{})
		end
	end
	return RUNNING
end
