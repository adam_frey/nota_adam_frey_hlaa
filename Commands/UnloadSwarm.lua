function getInfo()
	return {
		tooltip = "Unload units in position",
		parameterDefs = {
			{ 
				name = "atlases",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "pos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	local radius = 100
	-- get transport
	local transports = parameter.atlases
	-- done?
	local done = true
	for i=1, #transports do
		local loaded = Spring.GetUnitIsTransporting(transports[i])
		if #loaded ~= 1 then
			done = false
		end
	end
	if done then
		self.in_process = false
		return SUCCESS
	end
	-- unload
	if (self.in_process ~= true) then
		for i=1, #transports do
			local x,y,z = Spring.GetUnitPosition(transports[i])
			SpringGiveOrderToUnit(transports[i], CMD.UNLOAD_UNIT, {x, y, z, radius}, {})
		end
		self.in_process = true
	end
	-- return 
	return RUNNING
end

function Reset(self)
	self.in_process = false
end