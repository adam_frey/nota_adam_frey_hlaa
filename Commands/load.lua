function getInfo()
	return {
		tooltip = "Un/load units in position",
		parameterDefs = {
			{ 
				name = "cargo_id",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "transport_id",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	-- get transport
	local transport = parameter.transport_id
	-- get cargo
	local cargo = parameter.cargo_id
	-- how many units are currently loaded?
	local loaded = Spring.GetUnitIsTransporting(transport)
	-- load
	if (#loaded == 1) then
		self.in_process = false
		return SUCCESS
	elseif (self.in_process ~= true) then
		SpringGiveOrderToUnit(transport, CMD.LOAD_UNITS, {cargo}, {})		
		self.in_process = true
	end
	-- return 
	return RUNNING
end

function Reset(self)
	self.in_process = false
end