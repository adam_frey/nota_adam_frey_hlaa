function getInfo()
	return {
		tooltip = "Stay in place",
		parameterDefs = {}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	for i=1,#units do
		local x,y,z = Spring.GetUnitPosition(units[i])
		SpringGiveOrderToUnit(units[i], CMD.MOVE, {x, y, z}, {})
	end
	return RUNNING
end
