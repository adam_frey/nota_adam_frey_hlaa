function getInfo()
	return {
		tooltip = "Move unit to given position",
		parameterDefs = {
			{ 
				name = "unit_id",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	-- get position of unit
	local unit = parameter.unit_id
	local pointX, pointY, pointZ = SpringGetUnitPosition(unit)
	-- create Vec3 for target position
	local position = parameter.position
	local tX = position.x
	local tY = position.y
	local tZ = position.z
	-- check success
	d = math.sqrt((pointX - tX) * (pointX - tX) + (pointZ - tZ) * (pointZ- tZ))
	if (d < 75) then
		return SUCCESS
	else
		SpringGiveOrderToUnit(unit, CMD.MOVE, position:AsSpringVector(), {})
		return RUNNING
	end
	return RUNNING
end
