function getInfo()
	return {
		tooltip = "Load units in position",
		parameterDefs = {
			{ 
				name = "atlases",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "boxes",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	-- get transport
	local transports = parameter.atlases
	-- get cargo
	local cargo = parameter.boxes
	local done = true
	-- how many should be loaded?
	local n = math.min(#transports, #cargo)
	-- finished?
	for i=1,n do
		-- how many units are currently loaded?
		local loaded = Spring.GetUnitIsTransporting(transport)
		-- load
		if (#loaded == 0) then
			done = false		
		end
	end
	if done then
		self.in_process = false
		return SUCCESS
	end
	-- load
	if (self.in_process ~= true) then
		for i=1,n do
			SpringGiveOrderToUnit(transports[i], CMD.LOAD_UNITS, {cargo[i]}, {})		
			self.in_process = true
		end
	end
	-- return 
	return RUNNING
end

function Reset(self)
	self.in_process = false
end