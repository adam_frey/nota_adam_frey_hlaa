function getInfo()
	return {
		tooltip = "Un/load units in position",
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "radius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unload",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	-- get position of target
	local x = parameter.position["x"]
	local y = parameter.position["y"]
	local z = parameter.position["z"]
	-- get radius
	local radius = parameter.radius
	-- how many units are currently loaded?
	local loaded = Spring.GetUnitIsTransporting(units[1])
	-- load or unload
	if (parameter.unload and #loaded == 0) then
		self.in_process = false
		return SUCCESS
	elseif (not parameter.unload and #loaded == 3) then
		self.in_process = false
		return SUCCESS
	elseif (parameter.unload) then
		SpringGiveOrderToUnit(units[1], CMD.UNLOAD_UNITS, {x, y, z, radius}, {})
	else
		SpringGiveOrderToUnit(units[1], CMD.LOAD_UNITS, {x, y, z, radius}, {})	
	end
	-- return 
	return RUNNING
end

function Reset(self)
	self.in_process = false
end