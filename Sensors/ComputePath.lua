local sensorInfo = {
	name = "ComputePath",
	desc = ""
}

local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetGroundHeight = Spring.GetGroundHeight
local SpringGetUnitDefID = Spring.GetUnitDefID

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(unit, destination, grid, step)
	-- Breadth-First Search
	local function bfs(s, e, grid)
		local pred = {} -- predecessors
		local visited = {}
		for i=1,#grid do
			pred[i] = {}
			visited[i] = {}
		end
		visited[e[1]][e[2]] = true
		local directions = {}
		local frontier = {e}
		local next = {}
		while #frontier > 0 do
			local n = frontier[1]
			table.remove(frontier, 1)
			local i = n[1]
			local j = n[2]
			-- define neighbours
			local t = {math.max(1,i-1), j}
			local b = {math.min(#grid, i+1), j}
			local l = {i, math.max(1,j-1)}
			local r = {i, math.min(#grid[1], j+1)}
			-- end?
			if i == s[1] and j == s[2] then
			-- if true then
				break
			end
			-- deal with neighbours
			if (grid[t[1]][t[2]] == true and visited[t[1]][t[2]] == nil) then
				pred[t[1]][t[2]] = n
				visited[t[1]][t[2]] = true
				table.insert(frontier, t)
			end
			if (grid[b[1]][b[2]] == true and visited[b[1]][b[2]] == nil) then
				pred[b[1]][b[2]] = n
				visited[b[1]][b[2]] = true
				table.insert(frontier, b)
			end
			if (grid[l[1]][l[2]] == true and visited[l[1]][l[2]] == nil) then
				pred[l[1]][l[2]] = n
				visited[l[1]][l[2]] = true
				table.insert(frontier, l)
			end
			if (grid[r[1]][r[2]] == true and visited[r[1]][r[2]] == nil) then
				pred[r[1]][r[2]] = n
				visited[r[1]][r[2]] = true
				table.insert(frontier, r)
			end
		end
		-- get path
		local path = {}
		local curr = s
		while curr ~= e do
			if curr == nil then
				return {}
			end
			table.insert(path,curr)
			curr = pred[curr[1]][curr[2]]
		end
		-- return result
		return path
	end
	-- get nearest point in grid for unit
	local pointX, pointY, pointZ = SpringGetUnitPosition(unit)
	local s = {math.floor(pointX / step)+1, math.floor(pointZ / step)+1}
	-- get nearest point in grid for destination
	local e = {math.floor(destination.x / step)+1, math.floor(destination.z / step)+1}
	-- compute grid-path
	local gp = bfs(s,e,grid)
	-- local gp = {}
	-- travel to first node in grid
	local path = {}
	local y = SpringGetGroundHeight(s[1] * step,s[2] * step)
	path[1] = Vec3(s[1] * step,y,s[2] * step)
	-- append path
	for i=1, #gp do
		p = gp[i]
		local x = (p[1]-1) * step
		local z = (p[2]-1) * step
		y = SpringGetGroundHeight(x,z)
		path[i+1] = Vec3(x,y,z)
	end
	-- travel from last node to destination
	path[#gp+2] = destination
	-- return result
	return path
end