local sensorInfo = {
	name = "GetBasePos",
	
	author = "PepeAmpere",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return current wind statistics
return function(units, pos)
	local function VisibleEnemyUnitIds() 
		local enemy_ids = {}
		local my_id = Spring.GetMyTeamID()
		local my_units = Spring.GetTeamUnits(Spring.GetLocalTeamID())
		local all_units = Spring.GetAllUnits()
		for i=1, #all_units do
			local uid = Spring.GetUnitTeam(all_units[i])
			local found = false
			for j=1, #my_units do
				if all_units[i] == my_units[j] or Spring.AreTeamsAllied(uid, my_id) then
					found = true
					break
				end
			end
			if not found then
				enemy_ids[#enemy_ids+1] = all_units[i]
			end
		end
		return enemy_ids
	end
	-- default val
	if pos == nil then
		pos = Vec3(0,0,0)
	end
	-- iterate through visible enemies
	local visible = VisibleEnemyUnitIds()
	for i=1, #units do
		local ux,uy,uz = Spring.GetUnitPosition(units[i])
		local u_pos = Vec3(ux,uy,uz)
		for j=1, #visible do
			local id = visible[j]
			local t = Spring.GetUnitDefID(id)
			if t ~= nil and UnitDefs[t].name == "shika" then
				local x,y,z = Spring.GetUnitPosition(id)
				if Vec3(x,y,z):Distance(u_pos) < 1500 then
					return Vec3(x,y,z)
				end
			end
		end
	end
	return pos
end