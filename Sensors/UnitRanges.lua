local sensorInfo = {
	name = "enemyRanges",
	desc = "Return ranges of all visible enemy units.",
	author = "PepeAmpere",
	date = "2017-05-16",
	license = "notAlicense",
}

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return enemy ranges
return function()	
	-- get all (visible) enemy IDs
	local enemies = Spring.GetTeamUnits(1)
	-- for each enemy get its weapon and then its range
	ranges = {}
	for i = 1, #enemies do 
		local enemy = enemies[i]
		local def_id = Spring.GetUnitDefID(enemy)
		local weapon = UnitDefs[def_id].weapons[1].weaponDef
		local range = WeaponDefs[weapon].range
		ranges[i] = range
	end
	-- return result
	return ranges
end