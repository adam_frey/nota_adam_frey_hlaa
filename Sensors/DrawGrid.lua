local sensorInfo = {
	name = "DrawGrid"
	
}

-- get mandatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other mandatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(grid, step_size)

	local key = 0
    for i = 1, #grid do
		local g = grid[i]
        for j = 1, #g do
			local val = g[j]
			local x = (i-1) * step_size
			local z = (j-1) * step_size
			local y = Spring.GetGroundHeight(x,z) + 50
			key = key+2
			if (Script.LuaUI('exampleDebug_update') and val == true) then
				Script.LuaUI.exampleDebug_update(
					key,
					{	-- data
					startPos = Vec3(x,y,z) + Vec3(-10,0,0), 
					endPos = Vec3(x,y,z) + Vec3(10,0,0)
					}
				)
				Script.LuaUI.exampleDebug_update(
					key+1, -- key
					{	-- data
					startPos = Vec3(x,y,z) + Vec3(0,0,-10), 
					endPos = Vec3(x,y,z) + Vec3(0,0,10)
					}
				)
			end
        end
    end 

end