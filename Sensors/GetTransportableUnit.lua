local sensorInfo = {
	name = "GetTransportableUnit",
	desc = ""
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local SpringGetUnitPosition = Spring.GetUnitPosition

return function(n, save_spot)
	local to_save = {}
	local function comp_fn(u1, u2) 
		local u1x, u1y, u1z = SpringGetUnitPosition(u1)
		local u2x, u2y, u2z = SpringGetUnitPosition(u2)
		local d1 = (u1x - save_spot.x) * (u1x - save_spot.x) + (u1z - save_spot.z) * (u1z - save_spot.z)
		local d2 = (u2x - save_spot.x) * (u2x - save_spot.x) + (u2z - save_spot.z) * (u2z - save_spot.z)
		return (d1 < d2)
	end
	-- iterate over all own units
	units = Spring.GetTeamUnits(Spring.GetLocalTeamID())
	k = 1
	for i=1,#units do
		if UnitDefs[Spring.GetUnitDefID(units[i])].isGroundUnit then
			to_save[k] = units[i]
			k = k + 1
		end
	end
	-- sort arr
	table.sort(to_save, comp_fn)
	-- return result
	return to_save[n]
end