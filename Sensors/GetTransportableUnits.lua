local sensorInfo = {
	name = "GetTransportableUnit",
	desc = ""
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function()
	local to_save = {}
	-- iterate over all own units
	units = Spring.GetTeamUnits(Spring.GetLocalTeamID())
	k = 1
	for i=1,#units do
		if UnitDefs[Spring.GetUnitDefID(units[i])].isGroundUnit then
			to_save[k] = units[i]
			k = k + 1
		end
	end
	-- return result
	return to_save
end