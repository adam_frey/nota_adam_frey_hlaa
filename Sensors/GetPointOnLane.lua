local sensorInfo = {
	name = "DivideUnits",
	desc = ""
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(lane)
	local function GetAllies() 
		local allies = {}
		local my_id = Spring.GetMyTeamID()
		local all_units = Spring.GetAllUnits()
		for i=1,#all_units do
			local id = Spring.GetUnitTeam(all_units[i])
			if Spring.AreTeamsAllied(id, my_id) and id ~= my_id then
				allies[#allies+1] = all_units[i]
			end
		end
		return allies
	end
	---

	local function lane2arr(lane)
		local arr = {}
		for i=1,#lane do
			arr[#arr+1] = lane[i]["position"]
		end
		return arr
	end
	---
	local points = lane2arr(lane)
	-- add more points
	local p = {}
	for i=1,#points-1 do
		local p1 = points[i]
		local p2 = points[i+1]
		p[#p+1] = p1
		for j=1,10 do -- add 10 points
			p[#p+1] = p1 + (p2-p1)*(j/10)
		end
	end
	p[#p+1] = points[#points]
	points = p
	--
	local k = 0
	allies = GetAllies()
	for i=1,#points-1 do 
		for j=1,#allies do
			local x,y,z = Spring.GetUnitPosition(allies[j])
			if Vec3(x,y,z):Distance(points[i]) < 250 then
				k = i
			end
		end
	end	
	return points[k]
end