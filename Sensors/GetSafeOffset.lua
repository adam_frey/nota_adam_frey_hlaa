local sensorInfo = {
	name = "GetSafeOffset",
	desc = ""
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(n, r)
	local t = (r * math.sqrt(2) / 2) - 5
	local d = 2*t/6
	local x = -t + d * ((n-1) % 7)
	local z = -t + d * (math.floor((n-1)/7) % 7)
	return Vec3(x,0,z)
end