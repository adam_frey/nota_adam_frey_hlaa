local sensorInfo = {
	name = "ComputeGrid",
	desc = ""
}

local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetGroundHeight = Spring.GetGroundHeight
local SpringGetUnitDefID = Spring.GetUnitDefID

return function(enemies, step)
	local grid = {}
	-- constants
	local steps_x = math.floor(Game.mapSizeX / step)
	local steps_z = math.floor(Game.mapSizeZ / step)
	-- ideally the value would be here replaced with actual height of flight for atlas,
	-- not found in UnitDefNames["armatlas"]
	local h = Vec3(0,300,0)
	-- compute enemy weapons and positions
	local weapons = {}
	local positions = {}
	local t = 1
	for id, data in pairs(enemies) do
		local def_id = data[1]
		local pos = data[2] -- Vec3
		-- weaponu
		local u_def = nil
		if def_id ~= nil then
			u_def = UnitDefs[def_id]
		end
		if u_def ~= nil and u_def.weapons[1] ~= nil then
			weapons[t] = WeaponDefs[u_def.weapons[1].weaponDef]
		end
		-- position
		positions[t] = pos
		t = t + 1 
	end
	-- compute grid
	for i=1, steps_x do
		grid[i] = {}
		for j=1, steps_z do
			local x = (i-1) * step
			local z = (j-1) * step
			local y = SpringGetGroundHeight(x,z)
			local pos = Vec3(x,y,z) + h
			local safe = true
			-- check position
			for i=1, #weapons do	
				-- check for range
				-- (assume only a single weapon)
				local range = 1200 -- default
				local e_h = positions[i].y
				if weapons[i] ~= nil then
					range = weapons[i].range
				-- else
				-- 	Spring.Echo("weapon def not found")
				end
				if positions[i]:Distance(pos) <= range and e_h - 50 <= pos.y then
					safe = false
				end	
				-- check for height	
				-- ...
			end
			grid[i][j] = safe
		end
	end
	-- filter grid
	local border = {}
	for i=1, steps_x do
		border[i] = {}
		for j=1, steps_z do
			local g = grid[i][j]
			local t = {math.max(1,i-1), j}
			local b = {math.min(#grid, i+1), j}
			local l = {i, math.max(1,j-1)}
			local r = {i, math.min(#grid[1], j+1)}
			if g and (not
				   (grid[t[1]][t[2]] == true and 
					grid[b[1]][b[2]]  == true and
					grid[l[1]][l[2]]  == true and 
					grid[r[1]][r[2]]  == true)) then
				border[i][j] = true
			end
		end
	end
	for i=1, steps_x do
		for j=1, steps_z do
			if border[i][j] == true then
				grid[i][j] = false
			end
		end
	end
	-- return result
	return grid
end













