local sensorInfo = {
	name = "DivideUnits",
	desc = ""
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(lane)
	local function GetAllies() 
		local allies = {}
		local my_id = Spring.GetMyTeamID()
		local all_units = Spring.GetAllUnits()
		for i=1,#all_units do
			local id = Spring.GetUnitTeam(all_units[i])
			if Spring.AreTeamsAllied(id, my_id) and id ~= my_id then
				allies[#allies+1] = all_units[i]
			end
		end
		return allies
	end
	local function lane2arr(lane)
		local arr = {}
		for i=1,#lane do
			arr[#arr+1] = lane[i]["position"]
		end
		return arr
	end
	-- "ClosestPointOnLineSegment"
	local function closest_d(px,py,x1,y1,x2,y2, max_d)
	  local dx,dy = x2-x1,y2-y1
	  local length = math.sqrt(dx*dx+dy*dy)
	  dx,dy = dx/length,dy/length
	  local k = dx*(px-x1) + dy*(py-y1)
	  local posOnLine = math.min(length, math.max(0,dx*(px-x1) + dy*(py-y1)))
	  local tx, ty = x1+posOnLine*dx,x2+posOnLine*dy
	  -- too far
	  if math.sqrt((tx - px)*(tx - px)+(ty - py)*(ty - py)) >= max_d then
		return nil
	  end
	  -- on a line but not on segment
	  if k < 0 or k > length then
		-- return nil, nil
		return nil
	  end
	  -- return x1+posOnLine*dx,x2+posOnLine*dy
	  return k
	end
	-- note: points[1] closest to friendly base
	local points = lane2arr(lane)
	local f = 0
	local d = 0
	local delta = 850
	local last_i = 0
	local i_max = 0
	for i=1,#points-1 do
		local p1 = points[i]
		local p2 = points[i+1]
		allies = GetAllies()
		for j=1,#allies do
			px,py,pz = Spring.GetUnitPosition(allies[j])
			if px ~= nil and pz ~= nil then
				d_ = closest_d(px,pz,p1.x,p1.z,p2.x,p2.z,delta)
				if d_ ~= nil then
					if last_i < i then
						d = 0
					end
					i_max = i
					last_i = i
					d = math.max(d,d_)
				end
			end
		end
	end
	local res = 0
	Spring.Echo(i_max)
	for i=1,i_max do
		local p1 = points[i]
		local p2 = points[i+1]
		res = res + math.sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.z-p2.z)*(p1.z-p2.z))
	end
	return (res + d)
end