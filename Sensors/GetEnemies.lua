local sensorInfo = {
	name = "GetEnemies",
	desc = ""
}

local SpringGetTeamUnits = Spring.GetTeamUnits
local SpringGetAllUnits = Spring.GetAllUnits
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetUnitDefID = Spring.GetUnitDefID

return function(enemies)
	-- instantiate the table if it doesn't exist
	if enemies == nil then
		enemies = {}
	end
	-- function returning ids of currently visible enemies
	local function VisibleEnemyUnitIds() 
		local enemy_ids = {}
		local my_units = SpringGetTeamUnits(Spring.GetLocalTeamID())
		local all_units = SpringGetAllUnits()
		for i=0, #all_units do
			local found = false
			for j=0, #my_units do
				if all_units[i] == my_units[j] then
					found = true
					break
				end
			end
			if not found then
				enemy_ids[#enemy_ids+1] = all_units[i]
			end
		end
		return enemy_ids
	end
	-- iterate through visible enemies and update their info
	visible = VisibleEnemyUnitIds()
	for i=1, #visible do
		local id = visible[i]
		-- get position 
		local pointX, pointY, pointZ = SpringGetUnitPosition(id)
		local pos = Vec3(pointX, pointY, pointZ)
		-- get unit definition id
		local unit_def = SpringGetUnitDefID(id)
		-- save to table if all values are non-nil
		pred = (pointX ~= nil and pointY ~= nil and pointZ ~= nil and unit_def ~= nil)
		if pred then
			enemies[id] = {unit_def, pos}
		elseif (pointX ~= nil and pointY ~= nil and pointZ ~= nil) then
			enemies[id] = {nil, pos}
		end
	end
	-- return updated table
	return enemies
end