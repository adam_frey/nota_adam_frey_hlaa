local sensorInfo = {
	name = "DivideUnits",
	desc = ""
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(groups)
	local function keyset(t)
		local ks = {}
		for k,v in pairs(t) do
			ks[#ks+1] = k
		end
		return ks
	end
	-- auxiliary function
	local function TableConcat(t1,t2)
		for i=1,#t2 do
			t1[#t1+1] = t2[i]
		end
		return t1
	end	
	-- iterate over all own units
	local units = Spring.GetTeamUnits(Spring.GetLocalTeamID())
	-- filter all units that are in groups
	local new_units = {}
	for i=1,#units do
		local add = true
		local id = Spring.GetUnitDefID(units[i])
		
		--if UnitDefs[id].isBuilding then
		--	add = false
		--end
		
		local j = "atlas"
		for k=1, #groups[j] do
			if groups[j][k] == units[i] then
				add = false
				break
			end
		end	
		local j = "box"
		for k=1, #groups[j] do
			if groups[j][k] == units[i] then
				add = false
				break
			end
		end
		local j = "farck"
		for k=1, #groups[j] do
			if groups[j][k] == units[i] then
				add = false
				break
			end
		end
		local j = "art"
		for k=1, #groups[j] do
			if groups[j][k] == units[i] then
				add = false
				break
			end
		end	
		local j = "zeus"
		for k=1, #groups[j] do
			if groups[j][k] == units[i] then
				add = false
				break
			end
		end
		local j = "spy"
		for k=1, #groups[j] do
			if groups[j][k] == units[i] then
				add = false
				break
			end
		end
	
		if add then
			new_units[#new_units+1] = units[i]
		end
	end
	-- divide new units among old groups
	for i=1,#new_units do
		if UnitDefs[Spring.GetUnitDefID(new_units[i])] == UnitDefNames["armatlas"] then
			groups["atlas"][#groups["atlas"]+1] = new_units[i]
		elseif UnitDefs[Spring.GetUnitDefID(new_units[i])] == UnitDefNames["armbox"] then
			groups["box"][#groups["box"]+1] = new_units[i]
		elseif UnitDefs[Spring.GetUnitDefID(new_units[i])] == UnitDefNames["armfark"] then
			groups["farck"][#groups["farck"]+1] = new_units[i]
		elseif UnitDefs[Spring.GetUnitDefID(new_units[i])] == UnitDefNames["armmart"] then
			groups["art"][#groups["art"]+1] = new_units[i]
		elseif UnitDefs[Spring.GetUnitDefID(new_units[i])] == UnitDefNames["armzeus"] then
			groups["zeus"][#groups["zeus"]+1] = new_units[i]
		elseif UnitDefs[Spring.GetUnitDefID(new_units[i])] == UnitDefNames["armspy"] then
			groups["spy"][#groups["spy"]+1] = new_units[i]
		end
	end
	-- remove dead units
	local j = "atlas"
	local new = {}
	for k=1, #groups[j] do
		if Spring.GetUnitIsDead(groups[j][k]) == false then
			new[#new+1] = groups[j][k]
		end
	end	
	groups[j] = new
	---
	local j = "box"
	local new = {}
	for k=1, #groups[j] do
		if Spring.GetUnitIsDead(groups[j][k]) == false then
			new[#new+1] = groups[j][k]
		end
	end	
	groups[j] = new
	---
	local j = "farck"
	local new = {}
	for k=1, #groups[j] do
		if Spring.GetUnitIsDead(groups[j][k]) == false then
			new[#new+1] = groups[j][k]
		end
	end	
	groups[j] = new
	---
	local j = "art"
	local new = {}
	for k=1, #groups[j] do
		if Spring.GetUnitIsDead(groups[j][k]) == false then
			new[#new+1] = groups[j][k]
		end
	end
	groups[j] = new	
	---
	local j = "zeus"
	local new = {}
	for k=1, #groups[j] do
		if Spring.GetUnitIsDead(groups[j][k]) == false then
			new[#new+1] = groups[j][k]
		end
	end
	groups[j] = new
	---
	local j = "spy"
	local new = {}
	for k=1, #groups[j] do
		if Spring.GetUnitIsDead(groups[j][k]) == false then
			new[#new+1] = groups[j][k]
		end
	end	
	groups[j] = new
	-- return result
	return groups
end