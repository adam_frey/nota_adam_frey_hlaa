local sensorInfo = {
	name = "GetBasePos",
	
	author = "PepeAmpere",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return current wind statistics
return function(units, pos)
	d = 1e+12
	for i=1,#units do
		x,y,z = Spring.GetUnitPosition(units[i])
		d = math.min(d,Vec3(x,y,z):Distance(pos))
	end
	return d
end