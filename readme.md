HLAA Nota Homeworks
===

HW1: Sandsail
---
Behaviour `sandsail`, that calls the command `follow_wind`.

Logo source: `https://www.shareicon.net/wind-97580`

HW2: ctp2
---
Behaviour `ctp2`. In all my runs, it has achieved bonus point solution.

Usage: Select all 7 units on the board, and apply the behaviour.

Logo source: `https://www.shareicon.net/mountains-nature-landscape-goal-mountain-824164`

HW3: ttdr
---

Logo source: `https://icons8.com/icons/set/rescue`


Exam: swampdota 
----
Behaviour `dota`.

Logo source: `https://www.prosportstickers.com/product_images/t/dota_game_logo__46959_thumb.jpg`